//
//  StudentListViewController.swift
//  CoreData_Demo
//
//  Created by Amol Tamboli on 13/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class StudentListViewController: UIViewController {

    @IBOutlet weak var tblList: UITableView!
    
    var studentArr = [Student]()
    var college : College?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblList.register(UITableViewCell.self, forCellReuseIdentifier: "studentCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if college?.students?.allObjects != nil {
           studentArr = college?.students?.allObjects as! [Student]
        }
        self.tblList.reloadData()
    }

    
    @IBAction func btnAddStudentClicked(_ sender: UIBarButtonItem) {
        let collegeFormVc = self.storyboard!.instantiateViewController(identifier: "StudentFormViewController") as! StudentFormViewController
        collegeFormVc.college = college
        self.navigationController?.pushViewController(collegeFormVc, animated: true)
    }
}

extension StudentListViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "studentCell", for: indexPath)
        
        if cell != nil {
            cell = UITableViewCell(style: .value2, reuseIdentifier: "studentCell")
        }
        
        cell.textLabel?.text = studentArr[indexPath.row].name
        cell.detailTextLabel?.text = studentArr[indexPath.row].phone
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
