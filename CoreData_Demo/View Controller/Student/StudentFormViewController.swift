//
//  StudentFormViewController.swift
//  CoreData_Demo
//
//  Created by Amol Tamboli on 13/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class StudentFormViewController: UIViewController {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    
    var college : College?

    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    @IBAction func btnSaveClicked(_ sender: Any) {
        guard let name = txtName.text else {return}
        guard let email = txtEmail.text else {return}
        guard let phone = txtPhone.text else {return}
        guard let mainCollege = college else {return}

        let studentDict = [
            "studentName" : name,
            "studentEmail" : email,
            "studentPhone" : phone
        ]
        DatabaseHelper.shareInstance.saveStudentData(studentDict: studentDict, college: mainCollege)
    }
    
}
