//
//  ViewController.swift
//  CoreData_Demo
//
//  Created by Amol Tamboli on 31/08/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class CollegeListViewController: UIViewController {
    
    @IBOutlet weak var tblCollegeList: UITableView!
    var arrCollege = [College]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        arrCollege = DatabaseHelper.shareInstance.getAllCollegeData()
        self.tblCollegeList.reloadData()
    }
    
    @IBAction func clickAddCollegeForm(_ sender: UIBarButtonItem) {
        let collegeForm = self.storyboard?.instantiateViewController(identifier: "CollegeFormViewController") as! CollegeFormViewController
        self.navigationController?.pushViewController(collegeForm, animated: true)
    }
    
}

extension CollegeListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCollege.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblCollegeList.dequeueReusableCell(withIdentifier: "CollegeListViewCell", for: indexPath) as! CollegeListViewCell
        cell.college = arrCollege[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let collegeDetailVC = self.storyboard?.instantiateViewController(identifier: "CollegeDetailViewController") as! CollegeDetailViewController
        collegeDetailVC.collegeDetail = arrCollege[indexPath.row]
        collegeDetailVC.indexRow = indexPath.row
        self.navigationController?.pushViewController(collegeDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            arrCollege = DatabaseHelper.shareInstance.deleteCollegeData(index: indexPath.row)
            self.tblCollegeList.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
}

