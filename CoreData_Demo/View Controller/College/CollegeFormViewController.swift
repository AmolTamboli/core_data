//
//  CollegeFormViewController.swift
//  Save_Get_Core_Data
//
//  Created by Amol Tamboli on 31/08/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class CollegeFormViewController: UIViewController {

    @IBOutlet weak var txtCollegeName: UITextField!
    @IBOutlet weak var txtCollegeUniversity: UITextField!
    @IBOutlet weak var txtCollegeCity: UITextField!
    @IBOutlet weak var txtCollegeAddress: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    var isUpdate = false
    var indexRow = Int()
    var collegeDetail : College?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtCollegeName.text = collegeDetail?.name
        self.txtCollegeCity.text = collegeDetail?.city
        self.txtCollegeAddress.text = collegeDetail?.address
        self.txtCollegeUniversity.text = collegeDetail?.university
        
        txtCollegeName.layer.borderColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        txtCollegeUniversity.layer.borderColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        txtCollegeCity.layer.borderColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        txtCollegeAddress.layer.borderColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        btnSave.layer.borderColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)

        txtCollegeName.layer.borderWidth = 2
        txtCollegeName.layer.cornerRadius = 5
        txtCollegeUniversity.layer.borderWidth = 2
        txtCollegeUniversity.layer.cornerRadius = 5
        txtCollegeCity.layer.borderWidth = 2
        txtCollegeCity.layer.cornerRadius = 5
        txtCollegeAddress.layer.borderWidth = 2
        txtCollegeAddress.layer.cornerRadius = 5
        btnSave.layer.borderWidth = 4
        btnSave.layer.cornerRadius = 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isUpdate {
            btnSave.setTitle("UPDATE", for: .normal)
        } else {
            btnSave.setTitle("SAVE", for: .normal)

        }
    }
}

extension CollegeFormViewController {
    
    @IBAction func btnSaveClicked(_ sender: UIButton) {
        self.collegeSaveData()
        self.navigationController?.popViewController(animated: true)
    }
}
 
extension CollegeFormViewController {
    func collegeSaveData(){
        guard let collegeName = txtCollegeName.text else {return}
        guard let collegeAddress = txtCollegeAddress.text else {return}
        guard let collegCity = txtCollegeCity.text else {return}
        guard let collegeUniversity = txtCollegeUniversity.text else {return}

        let collegeDict = [
            "collegeName" : collegeName,
            "collegeAddress" : collegeAddress,
            "collegCity" : collegCity,
            "collegeUniversity" : collegeUniversity
        ]
        
        if isUpdate {
            DatabaseHelper.shareInstance.editCollegeData(collegeDict: collegeDict, index: indexRow)
            isUpdate = false
        } else {
            DatabaseHelper.shareInstance.saveCollegeData(collegeDict: collegeDict)
        }
    }
}
