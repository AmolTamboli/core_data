//
//  CollegeDetailViewController.swift
//  CoreData_Demo
//
//  Created by Amol Tamboli on 01/09/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class CollegeDetailViewController: UITableViewController {
    
    @IBOutlet weak var lblCollegeName: UILabel!
    @IBOutlet weak var lblCollegeAddress: UILabel!
    @IBOutlet weak var lblCollegeCity: UILabel!
    @IBOutlet weak var lblCollegeUniversity: UILabel!
    @IBOutlet weak var lblCollegeStudents: UILabel!
    
    var collegeDetail : College?
    var indexRow = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lblCollegeName.text = collegeDetail?.name ?? ""
        lblCollegeCity.text = collegeDetail?.city ?? ""
        lblCollegeUniversity.text = collegeDetail?.university ?? ""
        lblCollegeAddress.text = collegeDetail?.address ?? ""
        
        if let students = collegeDetail?.students?.allObjects as? [Student]{
            lblCollegeStudents.text = "\(students.count)"
        } else {
            lblCollegeStudents.text = "0"
        }
    }
    
    @IBAction func btnEditClicked(_ sender: UIBarButtonItem) {
        let formVC = self.storyboard?.instantiateViewController(identifier: "CollegeFormViewController") as! CollegeFormViewController
        formVC.isUpdate = true
        formVC.collegeDetail = collegeDetail
        formVC.indexRow = indexRow
        self.navigationController?.pushViewController(formVC, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 4 {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let collegeListVc = storyboard.instantiateViewController(identifier: "StudentListViewController") as! StudentListViewController
            collegeListVc.college = collegeDetail
            self.navigationController?.pushViewController(collegeListVc, animated: true)
        }
    }
    
}
