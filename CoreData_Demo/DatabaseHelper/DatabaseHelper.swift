//
//  DatabaseHelper.swift
//  Save_Get_Core_Data
//
//  Created by Amol Tamboli on 31/08/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit
import CoreData

class DatabaseHelper: NSObject {
    
    static let shareInstance = DatabaseHelper()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func saveCollegeData (collegeDict: [String:String]){
        let college = NSEntityDescription.insertNewObject(forEntityName: "College", into: context) as! College
        college.name = collegeDict["collegeName"]
        college.address = collegeDict["collegeAddress"]
        college.city = collegeDict["collegCity"]
        college.university = collegeDict["collegeUniversity"]
        do {
            try context.save()
        } catch let err{
            print("College Save Error:- \(err.localizedDescription)")
        }
    }
    
    func getAllCollegeData() -> [College]{
        var arrCollege = [College]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "College")
        do {
            arrCollege = try context.fetch(fetchRequest) as! [College]
        } catch let err{
            print("Error In College Fetch:- \(err.localizedDescription)")
        }
        return arrCollege
    }
    
    func deleteCollegeData(index:Int) -> [College]{
        var collegeData = self.getAllCollegeData() // GET DATA
        context.delete(collegeData[index]) // REMOVE FROM CORE DATA
        collegeData.remove(at: index) // REMOVE IN ARRAY
        
        do{
            try context.save()
        } catch let err{
            print("Delete College Data Error:\(err.localizedDescription)")
        }
        
        return collegeData
    }
    
    func editCollegeData(collegeDict:[String:String], index:Int){
        let college = self.getAllCollegeData()
        college[index].name = collegeDict["collegeName"]
        college[index].address = collegeDict["collegeAddress"]
        college[index].city = collegeDict["collegCity"]
        college[index].university = collegeDict["collegeUniversity"]
        do{
            try context.save()
        } catch let err {
            print("Error in edit data")
        }
    }
    
    func saveStudentData (studentDict: [String:String], college : College){
        let student = NSEntityDescription.insertNewObject(forEntityName: "Student", into: context) as! Student

        student.name = studentDict["studentName"]
        student.email = studentDict["studentEmail"]
        student.phone = studentDict["studentPhone"]
        student.colleges = college
        do {
            try context.save()
        } catch let err{
            print("Student Save Error:- \(err.localizedDescription)")
        }
    }
    
    func getAllStudentData() -> [Student]{
        var arrStudent = [Student]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Student")
        do {
            arrStudent = try context.fetch(fetchRequest) as! [Student]
        } catch let err{
            print("Error In Student Fetch:- \(err.localizedDescription)")
        }
        return arrStudent
    }
    
}
